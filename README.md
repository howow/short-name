
# short-name

## Install

```
npm install bitbucket:howow/short-name
```

## Usage

```
const p = require('@hokang/short-name').debug(1);
p('test')
// test
```
