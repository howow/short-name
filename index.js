'use strict'

const fs = require('fs')
const {
  Console
} = require('console')

//_.partialRight(_.tap, console.log)
const noop = () => { }
const dump = (...args) => console.log(...args) || (args.length>1? args: args[0])
const fail = console.error.bind(console)
const debug = (yes) => yes ? dump : noop
const begin = console.time.bind(console)
const end = console.timeEnd.bind(console)
const measure = (key) => ({
  'begin': () => begin(key),
  'end': () => end(key),
  'not': { 'begin': noop, 'end': noop }
})

// custom simple logger
const Logs = {}
const log = (file, ...args) => {
  let logger = file ? Logs[file] : console

  if (!logger && file) {
    // let out = fs.createWriteStream(`./${file}.log`);
    // let err = fs.createWriteStream(`/dev/null`);
    // let err = fs.createWriteStream(`./${file}.err`);
    logger = Logs[file] = new Console(fs.createWriteStream(`./${file}.log`), null)
  }

  return args.length ? logger.log(...args) : logger.log.bind(logger)
}

module.exports = {
  'dump': dump,
  'fail': fail,
  'debug': debug,
  'log': log,
  'begin': begin,
  'end': end,
  'measure': measure
}
